@extends('layouts.app')

@extends('master')
<div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

@section('maincontent')
               
                
               

      <div class="content">
        
        <div class="row">
         
          
          <div class="col-md-6">
            
              
                
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>
                        Firstname
                      </th>
                      <th>
                        lastname
                      </th>
                      <th>
                        email
                      </th>
                      
                    </thead>
                    <tbody>
                      @foreach ($p as $user)

                      <tr>
                        <td>
                           {{ $user['firstname'] }}
                          
                        </td>
                        <td>
                           {{ $user['lastname'] }}
                        </td>
                        <td>
                            {{ $user['email'] }}
                        </td>
                        
                      </tr>
                 
                   
                 
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
     @endsection