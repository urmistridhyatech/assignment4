<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\student;
//use Mail;
use Validator;

class UserController extends Controller
{
     function adduser(Request $r)
    {
         $validateData=$r->validate(
            [
                'firstname' => 'required|min:3',
                'lastname' => 'required|min:3',
                'email' => 'required|email|unique:students',
                'password' => 'required|min:8|max:20',
                'address' => 'required|min:20',

            ],
        [
                'firstname.required' => ' The first name field is required.',
                'firstname.min' => ' The first name must be at least 5 characters.',
                'firstname.max' => ' The first name may not be greater than 35 characters.',
                'lastname.required' => ' The last name field is required.',
                'lastname.min' => ' The last name must be at least 5 characters.',
                'lastname.max' => ' The last name may not be greater than 35 characters.',
            ]);
        $email = $r->email;
    	$password=$r->password;
    	$firstname=$r->firstname;
    	$lastname=$r->lastname;
    	$address=$r->address;
        $gender=$r->gender;

        $photo=time().'.'.$r->photo->getClientOriginalExtension();
        $r->photo->move(public_path('photo'),$photo);


    	
    	$user=new student;
    	$user->email=$email;
    	$user->password=$password;
    	$user->firstname=$firstname;
    	$user->lastname=$lastname;
    	$user->address=$address;
        $user->gender=$gender;
        $user->photo=$photo;


    	$user->save();
       /* Mail::send(['text'=>'mail'],['name','urmi'],function($message)
        {
            $message->to('99urmishah@gmail.com','urmi')->subject('test email');
            $message->from('99urmishah@gmail.com','urmi');

        });*/
    	return redirect('admin');
    }

 	function viewuser()
    {
        $p=student::all()->toArray();

        return view("viewuser",compact('p'));
    }

    function master()
    {
        

        return view("master");
    }


        function index()
    {

        $p=student::all()->toArray();

        return view("home",compact('p'));
    }

    function userlogin(Request $r)
    {

        $email=$r->email;
        $password=$r->password;
      
       // echo "$nm $dis $img $post_id";
        
       // session(['email' => 'email'])
        $p=student::where(["email"=>$email])->get()->toArray();
       // print_r($p);
         if(($p[0]['email']==$email)&&($p[0]['password']==$password))
            {

                // $info=$r->session()->get('info');
                 //print_r($r->input());
                 $r->session()->put('info',$r->input());
                 //$info=$r->session()->get('info');
                // print_r($info['password']);
                  return redirect('userhome');

            }
            else
            {
                return redirect('userlogin');
            }
        
    }



      function updateuser(Request $r)
    {


        $email=$r->email;
    	$password=$r->password;
    	$firstname=$r->firstname;
    	$lastname=$r->lastname;
    	$address=$r->address;
    	$id=$r->id;
        $gender=$r->gender;

        $photo=time().'.'.$r->photo->getClientOriginalExtension();
        $r->photo->move(public_path('photo'),$photo);

       // echo "$nm $dis $img $post_id";
        




        $p= student::where('id', $id)
        	->update([
        		"email"=>$email,
        		'password'=>$password,
        		'firstname'=>$firstname, 
        		'lastname'=>$lastname,
                'gender'=>$gender,
                'photo'=>$photo
        	]);

        return redirect()->route('viewuser');
       
    }

    function edituser($id="")
    {
        $p=student::where(["id"=>$id])->get()->toArray();
        return view("edituser",compact('p'));
    }

      function deleteuser($id)
    {
        
        $deletedRows = student::where('id',$id)->delete();
        return redirect()->route('viewuser');
    }
       function userhome(Request $r)
    {
        
                $info=$r->session()->get('info');
                if($info!='')
                {
                 
                     return view("userhome");
                    }
                    else
                    {
                            return view("userlogin");
                    }
                // print_r($info['password']);
    }
       function editprofile(Request $r)
    {
        
                $info=$r->session()->get('info');
                if($info!='')
                {
                    $p=student::where(["password"=>$info['password']])->get()->toArray();
            //print_r($p);
       return view("editprofile",compact('p'));
                 

                }
                else
                {
                      return view("userlogin");
                }
                
           
    }
      function logout(Request $request)
    {
        
      //  $request->session()->forget('info');
               $request->session()->flush();
                      return view("userlogin");


           
    }
  function updateprofile(Request $r)
    {

         $validateData=$r->validate(
            [
                'firstname' => 'required|min:3',
                'lastname' => 'required|min:3',
                'email' => 'required|email|unique:students',
                'password' => 'required|min:8|max:20',
                'address' => 'required|min:20',

            ],
        [
                'firstname.required' => ' The first name field is required.',
                'firstname.min' => ' The first name must be at least 5 characters.',
                'firstname.max' => ' The first name may not be greater than 35 characters.',
                'lastname.required' => ' The last name field is required.',
                'lastname.min' => ' The last name must be at least 5 characters.',
                'lastname.max' => ' The last name may not be greater than 35 characters.',
            ]);
        $email=$r->email;
        $password=$r->password;
        $firstname=$r->firstname;
        $lastname=$r->lastname;
        $address=$r->address;
        $id=$r->id;
        $gender=$r->gender;

        $photo=time().'.'.$r->photo->getClientOriginalExtension();
        $r->photo->move(public_path('photo'),$photo);

       // echo "$nm $dis $img $post_id";
        
      //  echo "string";




        $p= student::where('id', $id)
            ->update([
                "email"=>$email,
                'password'=>$password,
                'firstname'=>$firstname, 
                'lastname'=>$lastname,
                'gender'=>$gender,
                'photo'=>$photo
            ]);

        return redirect()->route('editprofile');
       
    }
    
}
