<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;

class forAdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $email = Auth::User()->email;
       // echo $email;
           
        if($email !='')
        {
            return $next($request);            
        }
        else
        {
             return redirect('home');
        }

    }
}
