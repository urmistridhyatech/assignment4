<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();

Route::middleware(['auth','admin_role'])->group(function()
{

Route::get('/home', 'UserController@index')->name('index');

Route::get('/master', 'UserController@master')->name('master');


Route::get('adduser', function () {
    return view('adduser');
});

Route::get('adduser', function () {
    return view('adduser');
});


Route::get('/viewuser', 'UserController@viewuser')->name('viewuser');

Route::get('/deleteuser/{id}', 'UserController@deleteuser')->name('deleteuser');

Route::get("/edituser/{id}",'UserController@edituser')->name("edituser");


Route::post('/adduser', 'UserController@adduser')->name('adduser');

Route::post('/updateuser', 'UserController@updateuser')->name('updateuser');


Route::get('dashboard', function () {
    return view('dashboard');
});
});
/*Route::get('userhome', function () {
	  //session(['email' => 'email']);
    return view('userhome');
});*/


Route::get('/userhome', 'UserController@userhome')->name('userhome');

Route::get('/editprofile', 'UserController@editprofile')->name('editprofile');


Route::get('/logout', 'UserController@logout')->name('logout');




Route::post('/updateprofile', 'UserController@updateprofile')->name('updateprofile');



Route::get('userlogin', function () {
    return view('userlogin');
});
Route::post('/userlogin', 'UserController@userlogin')->name('userlogin');
